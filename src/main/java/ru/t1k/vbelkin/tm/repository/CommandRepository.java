package ru.t1k.vbelkin.tm.repository;

import ru.t1k.vbelkin.tm.api.repository.ICommandRepository;
import ru.t1k.vbelkin.tm.constant.ArgumentConst;
import ru.t1k.vbelkin.tm.constant.TerminalConst;
import ru.t1k.vbelkin.tm.model.Command;

public final class CommandRepository implements ICommandRepository {

    private final static Command ABOUT = new Command(
            TerminalConst.ABOUT, ArgumentConst.ABOUT, "Show developer info."
    );
    private final static Command HELP = new Command(
            TerminalConst.HELP, ArgumentConst.HELP, "Show application commands."
    );
    private final static Command VERSION = new Command(
            TerminalConst.VERSION, ArgumentConst.VERSION,
            "Show application version."
    );
    private final static Command EXIT = new Command(
            TerminalConst.EXIT, null,
            "Close Application."
    );
    private final static Command INFO = new Command(
            TerminalConst.INFO, ArgumentConst.INFO, "Show system info."
    );
    private final static Command PROJECT_CLEAR = new Command(
            TerminalConst.PROJECT_CLEAR, null, "Remove all projects."
    );
    private final static Command PROJECT_LIST = new Command(
            TerminalConst.PROJECT_LIST, null, "Display project list."
    );
    private final static Command PROJECT_CREATE = new Command(
            TerminalConst.PROJECT_CREATE, null, "Create new project."
    );
    private final static Command TASK_CLEAR = new Command(
            TerminalConst.TASK_CLEAR, null, "Remove all tasks."
    );
    private final static Command TASK_LIST = new Command(
            TerminalConst.TASK_LIST, null, "Display task list."
    );
    private final static Command TASK_CREATE = new Command(
            TerminalConst.TASK_CREATE, null, "Create new task."
    );
    private final static Command TASK_SHOW_BY_INDEX = new Command(
            TerminalConst.TASK_SHOW_BY_INDEX, null, "Show task by index."
    );
    private final static Command TASK_SHOW_BY_ID = new Command(
            TerminalConst.TASK_SHOW_BY_ID, null, "Show task by id."
    );
    private final static Command TASK_REMOVE_BY_INDEX = new Command(
            TerminalConst.TASK_REMOVE_BY_INDEX, null, "Remove task by index."
    );
    private final static Command TASK_REMOVE_BY_ID = new Command(
            TerminalConst.TASK_REMOVE_BY_ID, null, "Remove task by id."
    );
    private final static Command TASK_UPDATE_BY_INDEX = new Command(
            TerminalConst.TASK_UPDATE_BY_INDEX, null, "Update task by index."
    );
    private final static Command TASK_UPDATE_BY_ID = new Command(
            TerminalConst.TASK_UPDATE_BY_ID, null, "Update task by id."
    );
    private final static Command PROJECT_SHOW_BY_INDEX = new Command(
            TerminalConst.PROJECT_SHOW_BY_INDEX, null, "Show project by index."
    );
    private final static Command PROJECT_SHOW_BY_ID = new Command(
            TerminalConst.PROJECT_SHOW_BY_ID, null, "Show project by id."
    );
    private final static Command PROJECT_REMOVE_BY_INDEX = new Command(
            TerminalConst.PROJECT_REMOVE_BY_INDEX, null, "Remove project by index."
    );
    private final static Command PROJECT_REMOVE_BY_ID = new Command(
            TerminalConst.PROJECT_REMOVE_BY_ID, null, "Remove project by id."
    );
    private final static Command PROJECT_UPDATE_BY_INDEX = new Command(
            TerminalConst.PROJECT_UPDATE_BY_INDEX, null, "Update project by index."
    );
    private final static Command PROJECT_UPDATE_BY_ID = new Command(
            TerminalConst.PROJECT_UPDATE_BY_ID, null, "Update project by id."
    );
    private final static Command START_PROJECT_BY_ID = new Command(
            TerminalConst.START_PROJECT_BY_ID, null, "Start project by id."
    );
    private final static Command START_PROJECT_BY_INDEX = new Command(
            TerminalConst.START_PROJECT_BY_INDEX, null, "Start project by index."
    );
    private final static Command COMPLETE_PROJECT_BY_ID = new Command(
            TerminalConst.COMPLETE_PROJECT_BY_ID, null, "Complete project by id."
    );
    private final static Command COMPLETE_PROJECT_BY_INDEX = new Command(
            TerminalConst.COMPLETE_PROJECT_BY_INDEX, null, "Complete project by index."
    );
    private final static Command CHANGE_PROJECT_STATUS_BY_ID = new Command(
            TerminalConst.CHANGE_STATUS_PROJECT_BY_ID, null, "Change project status by id."
    );
    private final static Command CHANGE_PROJECT_STATUS_BY_INDEX = new Command(
            TerminalConst.CHANGE_STATUS_PROJECT_BY_INDEX, null, "Change project status by index."
    );
    private final static Command START_TASK_BY_ID = new Command(
            TerminalConst.START_TASK_BY_ID, null, "Start task by id."
    );
    private final static Command START_TASK_BY_INDEX = new Command(
            TerminalConst.START_TASK_BY_INDEX, null, "Start task by index."
    );
    private final static Command COMPLETE_TASK_BY_ID = new Command(
            TerminalConst.COMPLETE_TASK_BY_ID, null, "Complete task by id."
    );
    private final static Command COMPLETE_TASK_BY_INDEX = new Command(
            TerminalConst.COMPLETE_TASK_BY_INDEX, null, "Complete task by index."
    );
    private final static Command CHANGE_TASK_STATUS_BY_ID = new Command(
            TerminalConst.CHANGE_STATUS_TASK_BY_ID, null, "Change task status by id."
    );
    private final static Command CHANGE_TASK_STATUS_BY_INDEX = new Command(
            TerminalConst.CHANGE_STATUS_TASK_BY_INDEX, null, "Change task status by index."
    );

    private final static Command[] TERMINAL_COMMANDS = new Command[]{
            INFO, ABOUT, HELP, VERSION,

            PROJECT_CLEAR, PROJECT_LIST, PROJECT_CREATE,
            PROJECT_SHOW_BY_INDEX, PROJECT_SHOW_BY_ID, PROJECT_REMOVE_BY_INDEX,
            PROJECT_REMOVE_BY_ID, PROJECT_UPDATE_BY_INDEX, PROJECT_UPDATE_BY_ID,
            START_PROJECT_BY_ID, START_PROJECT_BY_INDEX, COMPLETE_PROJECT_BY_ID, COMPLETE_PROJECT_BY_INDEX,
            CHANGE_PROJECT_STATUS_BY_ID, CHANGE_PROJECT_STATUS_BY_INDEX,

            TASK_CLEAR, TASK_LIST, TASK_CREATE,
            TASK_SHOW_BY_INDEX, TASK_SHOW_BY_ID, TASK_REMOVE_BY_INDEX,
            TASK_REMOVE_BY_ID, TASK_UPDATE_BY_INDEX, TASK_UPDATE_BY_ID,
            START_TASK_BY_ID, START_TASK_BY_INDEX, COMPLETE_TASK_BY_ID, COMPLETE_TASK_BY_INDEX,
            CHANGE_TASK_STATUS_BY_ID, CHANGE_TASK_STATUS_BY_INDEX,

            EXIT
    };

    @Override
    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}
