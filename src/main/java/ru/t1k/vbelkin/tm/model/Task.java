package ru.t1k.vbelkin.tm.model;

import ru.t1k.vbelkin.tm.enumerated.Status;

import java.util.UUID;

public final class Task {

    private String id = UUID.randomUUID().toString();

    private String name = "";

    private String description = "";

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    private Status status = Status.NOT_STARTED;

    public Task(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String toString() {
        return name + " : " + description;
    }

}
